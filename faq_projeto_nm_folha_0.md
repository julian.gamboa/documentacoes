## FAQ (projeto NM)

# básico

- *nrPedido*: campo "DocNum" da tabela de pedidos (no sistema SAP)
- *cdAdquirente*: usa-se como "1" se o cliente for trabalhar somente com a CIELO
- *nrNSU:* NSU é retornado pela CIELO
- **Antes de executar este ENDPOINT** *precisamos da quantidade de parcelas da venda* (proporcionado pela Adquirente)
- **Antes de executar este ENDPOINT** deve-se ter um "array" que indica se o cliente está pagando com mais de um cartão, considerando que cada elemento de dito "array" representa uma cartão.


```json
{
"nrPedido": "169", 
"cdCliente": "C00021",
"vlPedido": 1.00,
"pagamentos": [
{
"cdAdquirente": "1", 
"vlPagamento": 40,
"qtParcelas": 1,
"nrNSU": "123456"
},
{
"cdAdquirente": "1",
"vlPagamento": 60,
"qtParcelas": 2,
"nrNSU": "123457"
}
]
}
```

# Cuidados

É preciso manter o cliente como "ativo" no cadastro SAP

